import Component from '@glimmer/component';

export default class FilterComponent extends Component {
  get results(){
    let { rentals, query } = this.args;

    if(query){
      rentals = rentals.filter(rental => rental.title.includes(query));
    }

    return rentals;
  }
}

